<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {  
            $table->bigIncrements('order_product_id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('product_id');                      
            $table->double('price',15,2);          
            $table->double('quantity',15,2);            
            $table->double('sub_total',15,2);  
            $table->foreign('order_id')->references('order_id')->on('orders'); 
            $table->foreign('product_id')->references('product_id')->on('products');          
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}

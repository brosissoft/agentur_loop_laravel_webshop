<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Support\Facades\Http;

use GuzzleHttp\Client;

class OrderController extends Controller
{
    public function addOrder(Request $request)
	{   
		$data = $request->validate([
			'customer_id'=> 'required',  
		]);      
		$order = Order::create($data); 
		return new OrderResource($order);
	}

	public function addProductToOrder(Request $request)
	{   
		$data = $request->validate([
			'order_id'=> 'required',           					 			 
		]);
		
		//get order_details
		$order=Order::where('order_id',$request->order_id)->first();
        if($order)
		{
			//add prodct to order as long as the order is not payed. 
			if($order->payed==null)
			{
					foreach($request->orderProducts as $orderProduct) 
					{   
						OrderProduct::create([  
							'order_id' =>$request->order_id,
							'product_id' => $orderProduct['product_id'],						
							'price' => $orderProduct['price'],	
							'quantity' => $orderProduct['quantity'],						
							'sub_total' => $orderProduct['price']*$orderProduct['quantity']				
						]);					
					}			
			}
			else{
				return response()->json([
					'errors' => [
						'order' => ['Order is paid. Cannot add products to order'],
					]
				], 422);

			}
			return new OrderResource($order);
		}
		else{
			return response()->json([
				'errors' => [
					'order' => ['Order does not exist'],
				]
			], 422);
		}
	}

	public function updateProductToOrder(Request $request)
	{   
		$data = $request->validate([
			'order_id'=> 'required',           					 			 
		]);
		
		//get order_details
		$order=Order::where('order_id',$request->order_id)->first();

		//add prodct to order as long as the order is not payed. 
		if($order->payed==null)
		{
			if($request->deleted_orders)
			{
			foreach ($request->deleted_orders as $deleted_order) 
				{
					if(!empty($deleted_order['order_product_id'])) 
					{
						OrderProduct::where('order_product_id',$deleted_order['order_product_id'])->forceDelete();
						
					}
				}
			}
			if($request->orderProducts)
			{
				foreach($request->orderProducts as $orderProduct) 
				{   
					$data =[  
						'order_id' =>$request->order_id,
						'product_id' => $orderProduct['product_id'],						
						'price' => $orderProduct['price'],	
						'quantity' => $orderProduct['quantity'],						
						'sub_total' => $orderProduct['price']*$orderProduct['quantity']				
					];		
					
					if(empty($orderProduct['order_product_id'])) 
					{
						OrderProduct::create($data);				
					}
					else 
					{
						OrderProduct::where('order_product_id',$orderProduct['order_product_id'])->update($data);						
					}

				}	
			}		
		}
		else{
			return response()->json([
                'errors' => [
                    'message' => ['Order is paid. Cannot add products to order'],
                ]
            ], 422);

		}
		return new OrderResource($order);
	}
	

	public function getOrders(Request $request)
	{
		$order = Order::get();
		return OrderResource::collection($order);  
	}

	public function getOrder(Request $request,$order_id)
	{
		$order = Order::where('order_id',$order_id)->first();
		 return new OrderResource($order);  
	}


	public function pay(Request $request)
	{
		$data = $request->validate([
			'order_id'=> 'required',           
			'customer_email'=> 'required',           
			'value'=> 'required'  
		]);      
		

		$response = Http::get('https://superpay.view.agentur-loop.com/pay');
		$response = json_decode($response->getBody(), true);
		//return $response['message'];

		if($response['message']!='Invalid request')	
		{
			//Modify payed in order table
			$dataModified=[
				'payed'=>'Payed'.$data['value']
			];
			Order::where('order_id',$data['order_id'])->update($dataModified);
			return 'Payment Successful';
		}	
		else{

			return response()->json([
                'errors' => [
                    'message' => ['Insufficient Funds.'],
                ]
            ], 422);
		}		
	}
}




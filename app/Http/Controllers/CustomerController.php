<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use App\Models\Order;

class CustomerController extends Controller
{
    //login & logout with token
    
    public function getCustomers(Request $request)
    {       
        $customer = Customer::get();
        return CustomerResource::collection($customer);
    }

    public function showCustomer(Request $request)
    {      
		$customer=Customer::where('customer_id','1')->first();             
        return new CustomerResource($customer);
    }

}

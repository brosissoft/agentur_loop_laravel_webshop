<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "customer_id"=>$this->customer_id, 
            "job_title" => $this->job_title, 
            "email" => $this->email, 
            "firstname_lastname" => $this->firstname_lastname, 
            "registered_since"=>$this->registered_since,                
            "phone"=>$this->phone                
            ];
    }
}



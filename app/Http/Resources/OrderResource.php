<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "order_id"=>$this->order_id, 
            "customer_id"=>$this->customer_id, 
            "customer" => new CustomerResource($this->customer),
            "payed"=>$this->payed ,
            "products"=>OrderProductResource::collection($this->OrderProducts)               
            ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "order_product_id"=>$this->order_product_id,          
            "order_id"=>$this->order_id,          
            "product_id"=>$this->order_id,  
            "price"=>$this->price, 
            "quantity"=>$this->quantity,            
            "sub_total"=>$this->sub_total                
            ];
    }
}


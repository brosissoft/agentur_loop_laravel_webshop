<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [      
        'job_title',
        'email',
        'firstname_lastname',
        'registered_since',
        'phone'
    ];

    protected $primaryKey = "customer_id";

   protected $dates=['deleted_at'];
}


    
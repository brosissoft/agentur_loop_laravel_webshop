<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [      
        'customer_id',
        'payed',       
    ];

    protected $primaryKey = "order_id";

   protected $dates=['deleted_at'];

   public function Customer()
   {
       return $this->belongsTo('App\Models\Customer','customer_id','customer_id');
   } 

   public function OrderProducts()
   {
       return $this->hasMany('App\Models\OrderProduct','order_id','order_id');
   } 

}


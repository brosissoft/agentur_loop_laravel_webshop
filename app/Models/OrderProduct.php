<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProduct extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [      
        'order_id',
        'product_id',       
        'price',       
        'quantity',       
        'sub_total',       
    ];

    protected $primaryKey = "order_product_id";
   protected $dates=['deleted_at'];
}

 
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\CustomerController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post("addOrder",[OrderController::class,'addOrder']);
Route::post("getOrders",[OrderController::class,'getOrders']);
Route::get("getOrder/{order_id}",[OrderController::class,'getOrder']);
Route::post("addProductToOrder",[OrderController::class,'addProductToOrder']);
Route::post("updateProductToOrder",[OrderController::class,'updateProductToOrder']);
Route::post("pay",[OrderController::class,'pay']);

Route::post("getCustomers",[CustomerController::class,'getCustomers']);
Route::post("showCustomer",[CustomerController::class,'showCustomer']);

